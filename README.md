# TopTier

Sort anything in tiers by what you like the most!
Use pictures or text and then drag and drop in the tier you want.

[Try it!](https://quazar-omega.codeberg.page/toptier/@pages/dist/)

## Run locally
Clone the repository:
```
$ git clone https://codeberg.org/quazar-omega/toptier.git
```
Install the dependencies:
```
$ pnpm install
```
Run it:
```bash
$ pnpm run dev
```