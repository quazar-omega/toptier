import { Component, For, createSignal } from 'solid-js'
import {
	DragDropProvider,
	DragDropSensors,
	DragOverlay,
	Id,
	SortableProvider,
	closestCenter,
} from '@thisbeyond/solid-dnd'

import styles from './styles/BottomBar.module.css'

import Item from './Item'

const BottomBar: Component = () => {
	const [items, setItems] = createSignal([1, 2, 3, 4])
	const [activeItem, setActiveItem] = createSignal(null)

	const ids = () => items()

	const onDragStart = ({ draggable }: any) => setActiveItem(draggable.id)

	const onDragEnd = ({ draggable, droppable }: any) => {
		if (draggable && droppable) {
			const currentItems = ids()
			const fromIndex = currentItems.indexOf(draggable.id)
			const toIndex = currentItems.indexOf(droppable.id)
			if (fromIndex !== toIndex) {
				const updatedItems = currentItems.slice()
				updatedItems.splice(toIndex, 0, ...updatedItems.splice(fromIndex, 1))
				setItems(updatedItems)
			}
		}
		setActiveItem(null)
	}

	return (
		<section id={styles.bar}>
			<DragDropProvider
				onDragStart={onDragStart}
				onDragEnd={onDragEnd}
				collisionDetector={closestCenter}
			>
				<DragDropSensors />
				<div id={styles.items} /*class="column grid-flow-col self-stretch"*/>
					<SortableProvider ids={ids()}>
						<For each={items()}>{(id: Id) => <Item id={id} content={id.toString()} />}</For>
					</SortableProvider>
				</div>
				<DragOverlay>
					<div>{activeItem()}</div>
				</DragOverlay>
			</DragDropProvider>
			<button
				id={styles.newitem}
				onClick={() => {
					const newItemId = items().length + 1
					setItems(items().concat([newItemId]))
				}}>+</button>
		</section>
	)
}

export default BottomBar