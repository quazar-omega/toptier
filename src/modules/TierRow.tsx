import { Component, createSignal } from 'solid-js'
import styles from './styles/TierRow.module.css'

import Item from './Item'

const TierRow: Component<{ name: string }> = (props) => {
	const [tier, setTier] = createSignal(props.name)
	const items = 'none'
	// <input type="text" value="{tier()}">
	return (
		<div class={styles.row}>
			<div class={styles.nameBox}>
				<div class={styles.buttonDrag}>⠿</div>
				<div class={styles.name}>
					<textarea cols="1" rows="1">{tier()}</textarea>
				</div>
			</div>
			<div class={styles.itemList}>{items}</div>
		</div>
	)
}

export default TierRow