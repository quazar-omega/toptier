import type { Component } from 'solid-js'

import logo from './logo.svg'
import styles from './styles/App.module.css'

import TierRow from './TierRow'
import BottomBar from './BottomBar'

const App: Component = () => {
	return (
		<div>
			<header class={styles.header}>
				<img src={logo} class={styles.logo} alt="logo" />
				<p>
					TopTier
				</p>
			</header>
			<main class={styles.main}>
				<TierRow name="A" />
				<TierRow name="B" />
			</main>
			<BottomBar />
		</div>
	)
}

export default App
