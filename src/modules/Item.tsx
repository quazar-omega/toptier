import { Component } from 'solid-js'
import styles from './styles/Item.module.css'
import { createSortable, Id } from '@thisbeyond/solid-dnd'

const Item: Component<{ id: Id, content: string }> = (props) => {
	const sortable = createSortable(props.id)

	return (
		<div
			use:sortable
			class={styles.item}
			style={sortable.isActiveDraggable ? 'opacity: 50%;' : ''}>
			{props.content}
		</div>
	)
}

export default Item